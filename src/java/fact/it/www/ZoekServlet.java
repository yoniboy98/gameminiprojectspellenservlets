/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fact.it.www;

import com.realdolmen.miniProjectSpellen.beans.Moeilijkheid;
import com.realdolmen.miniProjectSpellen.dataaccess.DASoort;
import com.realdolmen.miniProjectSpellen.dataaccess.DASpel;
import com.realdolmen.miniProjectSpellen.dataaccess.DAmoeilijkheid;
import com.realdolmen.miniProjectSpellen.dataaccess.DAuitlenen;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author YVDBL89
 */

@WebServlet(name = "ZoekServlet", urlPatterns = {"/ZoekServlet"}, initParams = {
    @WebInitParam(name = "url", value = "jdbc:oracle:thin:@localhost:1521:XE"),
    @WebInitParam(name = "login", value = "admin"),
    @WebInitParam(name = "password", value = "sql"),
    @WebInitParam(name = "driver", value = "oracle.jdbc.driver.OracleDriver")}) 
 

public class ZoekServlet extends HttpServlet {
    private DASoort dasoort = null;
    private DASpel daspel = null;
    private DAuitlenen daouitlenen = null;
    private DAmoeilijkheid damoeilijkheid = null;
            
            
    @Override
    public void init() throws ServletException {
        try {
            String url = getInitParameter("url");
            String login = getInitParameter("login");
            String password = getInitParameter("password");
            String driver = getInitParameter("driver");
            if(dasoort == null) {
                dasoort = new DASoort(url , login , password, driver);
            }
            
            if(daspel == null) {
                daspel = new DASpel(url , login , password , driver);
            }
            if(daouitlenen == null) {
                daouitlenen = new DAuitlenen(url, login, password, driver);
            }
            if(damoeilijkheid == null) {
                damoeilijkheid = new DAmoeilijkheid(url, login, password, driver);
            }
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
    }
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
           RequestDispatcher rd = null;
        if(request.getParameter("moeilijk") != null){
               ArrayList<Moeilijkheid> moei = damoeilijkheid.getMoeilijkheid();
            request.setAttribute("moei", moei);
            rd = request.getRequestDispatcher("overzichtspel.jsp");
        }
    
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
