
package com.realdolmen.miniProjectSpellen.controller;

import com.realdolmen.miniProjectSpellen.beans.Soort;
import com.realdolmen.miniProjectSpellen.beans.Spel;
import com.realdolmen.miniProjectSpellen.beans.Uitlening;
import com.realdolmen.miniProjectSpellen.dataaccess.DASoort;
import com.realdolmen.miniProjectSpellen.dataaccess.DASpel;
import com.realdolmen.miniProjectSpellen.dataaccess.DAuitlenen;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "ManageServlet", urlPatterns = {"/ManageServlet"}, initParams = {
    @WebInitParam(name = "url", value = "jdbc:oracle:thin:@localhost:1521:XE"),
    @WebInitParam(name = "login", value = "admin"),
    @WebInitParam(name = "password", value = "sql"),
    @WebInitParam(name = "driver", value = "oracle.jdbc.driver.OracleDriver")}) 
 


public class ManageServlet extends HttpServlet {
    
    private DASoort dasoort = null;
    private DASpel daspel = null;
    private DAuitlenen daouitlenen = null;

    @Override
    public void init() throws ServletException {
        try {
            String url = getInitParameter("url");
            String login = getInitParameter("login");
            String password = getInitParameter("password");
            String driver = getInitParameter("driver");
            if(dasoort == null) {
                dasoort = new DASoort(url , login , password, driver);
            }
            
            if(daspel == null) {
                daspel = new DASpel(url , login , password , driver);
            }
            if(daouitlenen == null) {
                daouitlenen = new DAuitlenen(url, login, password, driver);
            }
        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }
    }

 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd = null;
        if(request.getParameter("soortKnop") != null){
            Soort soort = dasoort.getSoort();
            request.setAttribute("soort", soort);
            rd = request.getRequestDispatcher("soort.jsp");
        }
    
         if(request.getParameter("spelKnop") !=null ) {
            Spel spel = daspel.getSpel();
            request.setAttribute("spel" , spel);
            rd = request.getRequestDispatcher("spel.jsp");
        }
      
          if(request.getParameter("keuzenknop") !=null) {
             int id = Integer.parseInt(request.getParameter("keuzenknop_inp"));
            Spel spel = daspel.getSpelnaarkeuze(id);
            request.setAttribute("spel" , spel);
            rd = request.getRequestDispatcher("Spel_1.jsp");
        }
          
          if(request.getParameter("allespellen") != null) {
              ArrayList<Spel> spel = daspel.getAlleSpellen();
              request.setAttribute("spellen", spel);
              rd = request.getRequestDispatcher("overzichtSpellen.jsp");
          }
          if(request.getParameter("detailsknop") != null){
            try {
                ArrayList<Spel> speldetails = daspel.findSpelDetails();
                request.setAttribute("speldetails", speldetails);
                rd = request.getRequestDispatcher("speldetails.jsp");
            } catch (SQLException ex) {
                Logger.getLogger(ManageServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
           if(request.getParameter("uitleenknop") != null){
                ArrayList<Uitlening> uitlenen = daouitlenen.getLeenDetails();
                request.setAttribute("uitlenen", uitlenen);
                rd = request.getRequestDispatcher("uitleningen.jsp");
            }
        rd.forward(request , response);

    } 
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
