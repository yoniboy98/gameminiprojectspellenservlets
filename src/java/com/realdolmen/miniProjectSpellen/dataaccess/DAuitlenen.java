/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.dataaccess;

import com.realdolmen.miniProjectSpellen.beans.Spel;
import com.realdolmen.miniProjectSpellen.beans.Uitlening;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author YVDBL89
 */
public class DAuitlenen {
    private final String url , login , password;

    
    
    public DAuitlenen (String url , String login , String password , String driver) throws ClassNotFoundException {
        Class.forName(driver);
        this.url = url;
        this.login = login;
        this.password = password;
    }
    
    public ArrayList<Uitlening> getLeenDetails() {
     ArrayList<Uitlening> uitlenen= new ArrayList<>();     
        
        try (
                Connection connection = DriverManager.getConnection(url, login, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM UITLENING ORDER BY UITLEENDATUM");){
            
                while (resultSet.next()) {
                   Uitlening uitlening = new Uitlening();
                    uitlening.setNr(resultSet.getInt("NR"));
                    uitlening.setSpelnr(resultSet.getInt("SPELNR"));
                    uitlening.setLenernr(resultSet.getInt("LENERNR"));
                    uitlening.setUitleendatum(resultSet.getDate("UITLEENDATUM"));
                    uitlening.setTerugbrengdatum(resultSet.getDate("TERUGBRENGDATUM"));
                    uitlenen.add(uitlening);
         
                    
                }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uitlenen;

        }
}
