/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.dataaccess;

import com.realdolmen.miniProjectSpellen.beans.Lener;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author YVDBL89
 */
public class DAlenen {
    private final String url , login , password;

    
    
    public DAlenen (String url , String login , String password , String driver) throws ClassNotFoundException {
        Class.forName(driver);
        this.url = url;
        this.login = login;
        this.password = password;
    }
    
    public Lener getLener() {
        Lener lenen = null;
        
        try (
                Connection connection = DriverManager.getConnection(url, login, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM LENER");){
            
                if (resultSet.next()) {
                    lenen = new Lener();
                    lenen.setNr(resultSet.getInt("NR"));
                    lenen.setLenernaam(resultSet.getString("LENERNAAM"));
                    lenen.setStraat(resultSet.getString("STRAAT"));
                    lenen.setHuisnr(resultSet.getInt("HUISNR"));
                    lenen.setBusnr(resultSet.getInt("BUSNR"));
                    lenen.setPostcode(resultSet.getInt("POSTCODE"));
                    lenen.setGemeente(resultSet.getString("GEMEENTE"));
                    lenen.setTelefoon(resultSet.getLong("TELEFOON"));
                    lenen.setEmail(resultSet.getString("EMAIL"));
                }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    return lenen;
        }}
    
