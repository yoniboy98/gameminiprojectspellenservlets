/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.dataaccess;

import com.realdolmen.miniProjectSpellen.beans.Moeilijkheid;
import com.realdolmen.miniProjectSpellen.beans.Soort;
import com.realdolmen.miniProjectSpellen.beans.Spel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class DASpel {
    private final String url , login , password;

    
    
    public DASpel (String url , String login , String password , String driver) throws ClassNotFoundException {
        Class.forName(driver);
        this.url = url;
        this.login = login;
        this.password = password;
    }
    
    public Spel getSpel() {
        Spel spel = null;
        
        try (
                Connection connection = DriverManager.getConnection(url, login, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM SPEL WHERE NR = 5");){
            
                if (resultSet.next()) {
                    spel = new Spel();
                    spel.setNr(resultSet.getInt("NR"));
                    spel.setNaam(resultSet.getString("NAAM"));
                    spel.setUitgever(resultSet.getString("UITGEVER"));
                    spel.setAuteur(resultSet.getString("AUTEUR"));
                    spel.setJaar_uitgifte(resultSet.getInt("JAAR_UITGIFTE"));
                    spel.setLeeftijd(resultSet.getString("LEEFTIJD"));
                    spel.setMin_spelers(resultSet.getInt("MIN_SPELERS"));
                    spel.setMax_spelers(resultSet.getInt("MAX_SPELERS"));
                    spel.setSpeelduur(resultSet.getString("SPEELDUUR"));
                    spel.setPrijs(resultSet.getDouble("PRIJS"));
                    spel.setAfbeelding(resultSet.getString("AFBEELDING"));
                    
                }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    return spel;
        }
    
       public Spel getSpelnaarkeuze(int id) {
        Spel spel = null;
        
        try {
                Connection connection = DriverManager.getConnection(url, login, password);
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM SPEL WHERE NR = ?");
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();
                
                while (resultSet.next()) {
                    spel = new Spel();
                    spel.setNr(resultSet.getInt("NR"));
                    spel.setNaam(resultSet.getString("NAAM"));
                    spel.setUitgever(resultSet.getString("UITGEVER"));
                    spel.setAuteur(resultSet.getString("AUTEUR"));
                    spel.setJaar_uitgifte(resultSet.getInt("JAAR_UITGIFTE"));
                    spel.setLeeftijd(resultSet.getString("LEEFTIJD"));
                    spel.setMin_spelers(resultSet.getInt("MIN_SPELERS"));
                    spel.setMax_spelers(resultSet.getInt("MAX_SPELERS"));
                    spel.setSpeelduur(resultSet.getString("SPEELDUUR"));
                    spel.setPrijs(resultSet.getDouble("PRIJS"));
                    spel.setAfbeelding(resultSet.getString("AFBEELDING"));
                    
                }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    return spel;
        }
       
        public ArrayList<Spel> getAlleSpellen() {
      ArrayList<Spel> spellen = new ArrayList<>();
        
        try (
                Connection connection = DriverManager.getConnection(url, login, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM SPEL ORDER BY naam");){
            
                while (resultSet.next()) {
                    Spel spel = new Spel();
                    spel.setNr(resultSet.getInt("NR"));
                    spel.setNaam(resultSet.getString("NAAM"));
                    spel.setUitgever(resultSet.getString("UITGEVER"));
                    spel.setAuteur(resultSet.getString("AUTEUR"));
                    spel.setJaar_uitgifte(resultSet.getInt("JAAR_UITGIFTE"));
                    spel.setLeeftijd(resultSet.getString("LEEFTIJD"));
                    spel.setMin_spelers(resultSet.getInt("MIN_SPELERS"));
                    spel.setMax_spelers(resultSet.getInt("MAX_SPELERS"));
                    spel.setSpeelduur(resultSet.getString("SPEELDUUR"));
                    spel.setPrijs(resultSet.getDouble("PRIJS"));
                    spel.setAfbeelding(resultSet.getString("AFBEELDING"));
                    spellen.add(spel);
                }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    return spellen;
        }
        
  
           
          public ArrayList<Spel> findSpelDetails() throws SQLException {
            ArrayList<Spel> spel = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, login, password)) {
            PreparedStatement statement = connection.prepareStatement("select * from spel s inner join soort srt on s.soortnr = srt.nr inner join moeilijkheid  m on s.moeilijkheidnr = m.nr");
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                Spel  spellen = fullSetToData(set);
            } else {
                throw new SQLException("no records were found.");
            }
            set.close();
            statement.close();
        }
        return spel;
    }
          private Spel fullSetToData(ResultSet set) throws SQLException {
        return new Spel(set.getInt("spel.nr"),
                set.getString("spel.naam"),
                set.getString("spel.uitgever"),
                set.getString("spel.auteur"),
                set.getInt("spel.jaar_uitgifte"),
                set.getString("spel.leeftijd"),
                set.getInt("spel.min_spelers"),
                set.getInt("spel.max_spelers"),
                new Moeilijkheid(
                        set.getInt("moeilijkheid.nr"),
                        set.getString("moeilijkheid.moeilijkheidnaam")
                ),
                new Soort(
                        set.getInt("soort.nr"),
                        set.getString("soort.soortnaam")
                ),
                set.getString("spel.speelduur"),
                set.getDouble("spel.prijs"),
                set.getString("spel.afbeelding")
        );
    }


}
   

    
    
    
    

