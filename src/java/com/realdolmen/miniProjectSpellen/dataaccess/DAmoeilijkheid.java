/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.dataaccess;

import com.realdolmen.miniProjectSpellen.beans.Moeilijkheid;
import com.realdolmen.miniProjectSpellen.beans.Spel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author YVDBL89
 */
public class DAmoeilijkheid {
        private final String url , login , password;

    
    
    public DAmoeilijkheid (String url , String login , String password , String driver) throws ClassNotFoundException {
        Class.forName(driver);
        this.url = url;
        this.login = login;
        this.password = password;
    }
    
    public ArrayList<Moeilijkheid> getMoeilijkheid() {
              ArrayList<Moeilijkheid> moeilijk = new ArrayList<>();
        
        try (
                Connection connection = DriverManager.getConnection(url, login, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM MOEILIJKHEID ");){
            
                while (resultSet.next()) {
                Moeilijkheid  moeilijken = new Moeilijkheid();
                    moeilijken.setNr(resultSet.getInt("NR"));
                    moeilijken.setMoeilijkheidnaam(resultSet.getString("MOEILIJKHEIDNAAM"));
                    moeilijk.add(moeilijken);
                    
                   
                    
                }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    return moeilijk;
        }
}
