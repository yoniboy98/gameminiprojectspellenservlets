/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.beans;

import java.sql.Date;

/**
 *
 * @author DVMBM50
 */
public class Uitlening {
    private int nr;
    private int spelnr;
    private int lenernr;
    private Date uitleendatum;
    private Date terugbrengdatum;
    
    	public Uitlening (int nr, int spelnr, int lenernr, Date uitleendatum, Date terugbrengdatum) {
		super();
		this.nr = nr;
		this.spelnr = spelnr;
		this.lenernr = lenernr;
		this.uitleendatum = uitleendatum;
		this.terugbrengdatum = terugbrengdatum;
	}

    public Uitlening() {

    }

	public int getNr() {
		return nr;
	}
	public void setNr(int nr) {
		this.nr = nr;
	}
	public int getSpelnr() {
		return spelnr;
	}
	public void setSpelnr(int spelnr) {
		this.spelnr = spelnr;
	}
	public int getLenernr() {
		return lenernr;
	}
	public void setLenernr(int lenernr) {
		this.lenernr = lenernr;
	}
	public Date getUitleendatum() {
		return uitleendatum;
	}
	public void setUitleendatum(Date uitleendatum) {
		this.uitleendatum = uitleendatum;
	}
	public Date getTerugbrengdatum() {
		return terugbrengdatum;
	}
	public void setTerugbrengdatum(Date terugbrengdatum) {
		this.terugbrengdatum = terugbrengdatum;
	}
        
        	public String toString() {
			return "Food [id=" + nr + ", name=" + spelnr + ", price=" + lenernr + ", kcal=" + uitleendatum + ", duration="
					+ terugbrengdatum +"]";
		}
    
}
    

