/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.beans;

/**
 *
 * @author DVMBM50
 */
public class Spel {
    private int nr;
    private String naam;
    private String uitgever;
    private String auteur;
    private int jaar_uitgifte;
    private String leeftijd;
    private int min_spelers;
    private int max_spelers;
    private String speelduur;
    private Double prijs;
    private String afbeelding;

    public Spel(int nr, String naam, String uitgever, String auteur, int jaar_uitgifte, String leeftijd, int min_spelers, int max_spelers, String speelduur, Double prijs, String afbeelding) {
        this.nr = nr;
        this.naam = naam;
        this.uitgever = uitgever;
        this.auteur = auteur;
        this.jaar_uitgifte = jaar_uitgifte;
        this.leeftijd = leeftijd;
        this.min_spelers = min_spelers;
        this.max_spelers = max_spelers;
        this.speelduur = speelduur;
        this.prijs = prijs;
        this.afbeelding = afbeelding;
    }

    public Spel() {
    }

    public Spel(int aInt, String string, String string0, String string1, int aInt0, String string2, int aInt1, int aInt2, Moeilijkheid moeilijkheid, Soort soort, String string3, double aDouble, String string4) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getUitgever() {
        return uitgever;
    }

    public void setUitgever(String uitgever) {
        this.uitgever = uitgever;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getJaar_uitgifte() {
        return jaar_uitgifte;
    }

    public void setJaar_uitgifte(int jaar_uitgifte) {
        this.jaar_uitgifte = jaar_uitgifte;
    }

    public String getLeeftijd() {
        return leeftijd;
    }

    public void setLeeftijd(String leeftijd) {
        this.leeftijd = leeftijd;
    }

    public int getMin_spelers() {
        return min_spelers;
    }

    public void setMin_spelers(int min_spelers) {
        this.min_spelers = min_spelers;
    }

    public int getMax_spelers() {
        return max_spelers;
    }

    public void setMax_spelers(int max_spelers) {
        this.max_spelers = max_spelers;
    }

    public String getSpeelduur() {
        return speelduur;
    }

    public void setSpeelduur(String speelduur) {
        this.speelduur = speelduur;
    }

    public Double getPrijs() {
        return prijs;
    }

    public void setPrijs(Double prijs) {
        this.prijs = prijs;
    }

    public String getAfbeelding() {
        return afbeelding;
    }

    public void setAfbeelding(String afbeelding) {
        this.afbeelding = afbeelding;
    }

    @Override
    public String toString() {
        return "Spel{" + "nr=" + nr + ", naam=" + naam + ", uitgever=" + uitgever + ", auteur=" + auteur + ", jaar_uitgifte=" + jaar_uitgifte + ", leeftijd=" + leeftijd + ", min_spelers=" + min_spelers + ", max_spelers=" + max_spelers + ", speelduur=" + speelduur + ", prijs=" + prijs + ", afbeelding=" + afbeelding + '}';
    }

    

    
    
}