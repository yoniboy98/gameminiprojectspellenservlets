/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.beans;


public class Moeilijkheid {
    private int nr;
    private String moeilijkheidnaam;
    
        public Moeilijkheid( int nr, String moeilijkheidnaam) {
        this.nr = nr;
        this.moeilijkheidnaam = moeilijkheidnaam;
    }
        public Moeilijkheid() {
            
        }

  
          public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }
    
     public String getMoeilijkheidnaam() {
        return moeilijkheidnaam;
    }

    public void setMoeilijkheidnaam(String moeilijkheidnaam) {
        this.moeilijkheidnaam = moeilijkheidnaam;
    }
}
