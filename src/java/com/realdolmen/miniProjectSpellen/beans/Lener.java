/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.miniProjectSpellen.beans;

/**
 *
 * @author DVMBM50
 */
public class Lener {
    private int nr;
    private String lenernaam;
    private String straat;
    private int huisnr;
    private int busnr;
    private int postcode;
    private String gemeente;
    private long telefoon;
    private String email;

    public Lener(int nr, String lenernaam, String straat, int huisnr, int busnr, int postcode, String gemeente, long telefoon, String email) {
        this.nr = nr;
        this.lenernaam = lenernaam;
        this.straat = straat;
        this.huisnr = huisnr;
        this.busnr = busnr;
        this.postcode = postcode;
        this.gemeente = gemeente;
        this.telefoon = telefoon;
        this.email = email;
    }

    public Lener() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getLenernaam() {
        return lenernaam;
    }

    public void setLenernaam(String lenernaam) {
        this.lenernaam = lenernaam;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public int getHuisnr() {
        return huisnr;
    }

    public void setHuisnr(int huisnr) {
        this.huisnr = huisnr;
    }

    public int getBusnr() {
        return busnr;
    }

    public void setBusnr(int busnr) {
        this.busnr = busnr;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getGemeente() {
        return gemeente;
    }

    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }

    public long getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(long telefoon) {
        this.telefoon = telefoon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
