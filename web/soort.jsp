<%-- 
    Document   : Soort
    Created on : Feb 5, 2020, 12:57:25 PM
    Author     : DVMBM50
--%>

<%@page import="com.realdolmen.miniProjectSpellen.beans.Soort"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% Soort soort = (Soort) request.getAttribute("soort");%>
        <div id="headercontainer">
            <div id="header">
                <h1>Eerste Soort</h1>
            </div>
        </div>
        
        <div id="content">
            <h1><%=soort.getSoortnaam()%></h1>
            <p> Eerste Soortnaam = "<%=soort.getSoortnaam()%>" </p>
            
            <p> <a href="index.jsp"> Terug naar Home Page</a></p>
        </div>
    </body>
</html>
